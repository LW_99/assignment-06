#include<stdio.h>

int fibonacciSeq(int n)
{
    if (n==0){
        return 0;
    }
    if (n==1){
        return 1;
    }
    return fibonacciSeq(n-1) + fibonacciSeq(n-2);
}

int main ()
{
    int n;
    for (n=0 ; n <=10; n++){
      printf ("%d\t\n" , fibonacciSeq(n));
    }
    return 0;
}
